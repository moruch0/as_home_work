import 'dart:ui';

class AppColors{
  static const primary = Color(0xFFE5E5E5);
  static const gray_3 = Color(0xFF828282);
  static const gray_4 = Color(0xFFBDBDBD);
  static const primaryText = Color(0xFF0B1E2D);
  static const icon = Color(0xFF5B6975);
  static const black = Color(0xFF000000);
  static  const green_2 = Color(0xFF27AE60);
  static const red = Color(0xFFEB5757);
  static const gray_6 = Color(0xFFF2F2F2);
  static const primaryIndigo = Color(0xFF22A2BD);
  static const bottomNavigationBar = Color(0xFFFFFFFF);
  static const basicInactiveTab = Color(0xFFA3A3A3);
  static const background = Color(0xFFffffff);
}