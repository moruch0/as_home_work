import 'package:as_lesson_1/constants/app_colors.dart';
import 'package:flutter/material.dart';

class AppStyles {
  static const mediumText = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w500,
    color: AppColors.primaryText,
  );
  static const additionalText = TextStyle(
    fontSize: 10,
    fontWeight: FontWeight.w500,
    color: AppColors.green_2,
  );
  static const subtitleText = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.w400,
    color: AppColors.gray_3,
  );
  static const primaryText = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w400,
    color: AppColors.primaryText,
  );
  static const topTitle = TextStyle(
    fontSize: 10,
    fontWeight: FontWeight.w500,
    color: AppColors.gray_3,
  );
  static const settingsText = TextStyle(
    fontSize: 25,
    fontWeight: FontWeight.bold,
    color: AppColors.black,
  );
  static const label = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w400,
    color: AppColors.black,
  );
}
