class LocationDto {
  LocationDto({
    this.id,
    this.name,
    this.type,
    this.dimension,
    this.residents,
    this.url,
    this.created,
  });

  final int? id;
  final String? name;
  final String? type;
  final String? dimension;
  final List<String>? residents;
  final String? url;
  final DateTime? created;

  factory LocationDto.fromJson(Map<String, dynamic> json) => LocationDto(
        id: json["id"],
        name: json["name"],
        type: json["type"],
        dimension: json["dimension"],
        residents: json["residents"] == null ? null : List<String>.from(json["residents"]),
        url: json["url"],
        created: json["created"] == null ? null : DateTime.parse(json["created"]),
      );
}
