import 'package:as_lesson_1/common/models/location_model.dart';

class CharacterDto {
  CharacterDto({
    this.id,
    this.name,
    this.status,
    this.species,
    this.type,
    this.gender,
    this.origin,
    this.location,
    this.image,
    this.episode,
    this.url,
    this.created,
  });

  final DateTime? created;
  final List<String>? episode;
  final String? gender;
  final int? id;
  final String? image;
  final LocationDto? location;
  final String? name;
  final LocationDto? origin;
  final String? species;
  final String? status;
  final String? type;
  final String? url;

  factory CharacterDto.fromJson(Map<String, dynamic>? json) {
    if (json == null) return CharacterDto();
    return CharacterDto(
      id: json["id"],
      name: json["name"],
      status: json["status"],
      species: json["species"],
      type: json["type"],
      gender: json["gender"],
      origin: json["origin"] == null ? null : LocationDto.fromJson(json["origin"]),
      location: json["location"] == null ? null : LocationDto.fromJson(json["location"]),
      image: json["image"],
      episode: (json["episode"] as List).map((e) => e as String).toList(),
      url: json["url"],
      created: json["created"] == null ? null : DateTime.parse(json["created"]),
    );
  }
}
