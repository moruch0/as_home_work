import 'package:shared_preferences/shared_preferences.dart';

class RepoSettings {
  SharedPreferences? prefs;
//Объявили переменную  
  Future<void> init() async {
    prefs = await SharedPreferences.getInstance();
  }
//Функция для инициализации класса
  Future<bool?> saveLocal(String locale) async {
    if (prefs == null) return false;
    return prefs?.setString('locale', locale);
  }
//Устанавливаем значение локализации
  Future<String?> readLocal() async {
     if (prefs == null) return null;
    return prefs?.getString('locale');
  }
  //Получение значения локализвции
}
