import 'package:as_lesson_1/common/models/character_model.dart';
import 'package:as_lesson_1/common/repository/api.dart';
import 'package:as_lesson_1/generated/l10n.dart';
import 'package:as_lesson_1/model/characters.dart';

// class RepoCharacters{
// Future<ResultatRepoCharacters> readCharacters() async{
//   try {
//     final url = Uri.parse("https://rickandmortyapi.com/api/character");
//     final result = await http.get(url);
//     final data = jsonDecode(result.body);
//     final charatersListJson = data['results'] as List;
//     final charactersList = charatersListJson.map((e) => CharacterDto.fromJson(e)).toList();
//     return ResultatRepoCharacters(characterList: charactersList );
//   } catch (error) {
//      print('🏐 Error : $error');
//      return ResultatRepoCharacters(errorMesage: S.current.somethingWentWrong);
//   }
// }

// Future<ResultatRepoCharacters> filterByName(String name) async {
// try {
//    final url = Uri.parse("https://rickandmortyapi.com/api/character/?name$name");
//     final result = await http.get(url);
//     final data = jsonDecode(result.body);
//     final List charactersListJson = data['results'] ?? [];
//     final charactersList = charactersListJson
//     .map((item) => CharacterDto.fromJson(item),
//     )
//     .toList();
//     return ResultatRepoCharacters(characterList: charactersList);
// } catch (error) {
//      print('🏐 Error : $error');
//      return ResultatRepoCharacters(errorMesage: S.current.somethingWentWrong);
//   }
// }
// }

// class ResultatRepoCharacters{
//   ResultatRepoCharacters({this.errorMesage, this.characterList});
//   final String? errorMesage;
//   final List<CharacterDto>? characterList;
// }

class RepoCharacters {
  RepoCharacters({required this.api});

  final Api api;

  Future<ResultRepoCharacters> filterByName(String name) async {
    try {
      final result = await api.dio.get(
        '/character/',
        queryParameters: {
          "name": name,
        },
      );

      final List characterListJson = result.data['results'] ?? [];

      final characterList = characterListJson
          .map(
            (item) => CharacterDto.fromJson(item),
          )
          .toList();

      return ResultRepoCharacters(characterList: characterList);
      // вот эта строчка я думаю всё из за этого
    } catch (error) {
      print('🏐 Error : $error');
      return ResultRepoCharacters(
        errorMesage: S.current.somethingWentWrong,
      );
    }
  }
}

class ResultRepoCharacters {
  ResultRepoCharacters({
    this.errorMesage,
    this.characterList,
  });

  final String? errorMesage;
  final List<CharacterDto>? characterList;
}
