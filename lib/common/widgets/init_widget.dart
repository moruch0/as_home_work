import 'package:as_lesson_1/common/repository/api.dart';
import 'package:as_lesson_1/common/repository/repo_characters.dart';
import 'package:as_lesson_1/common/repository/repo_settings.dart';
import 'package:as_lesson_1/screens/characters_screen/src/bloc/bloc_characters.dart';
import 'package:as_lesson_1/widgets/app_nav_bar/view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

// class InitWidget extends StatelessWidget {
//   const InitWidget({
//     Key? key,
//     required this.child,
//   }) : super(key: key);

//   final Widget child;

//   @override
//   Widget build(BuildContext context) {
//     return MultiProvider(
//       providers: [
//         Provider(create: (context) => RepoSettings()),
//         Provider(create: (context) => RepoSettings()),
//       ],
//       child: MultiBlocProvider(
//         providers: [
//           BlocProvider(
//             create: (context) => BlockCharacter(
//               repo: RepositoryProvider.of<RepoCharacters>(context),
//             )..add(EventCharactersFilterByName('')),
//           )
//         ],
//         child: child,
//       ),
//     );
//   }
// }

class InitWidget extends StatelessWidget {
  const InitWidget({
    Key? key,
    required this.child,
  }) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        RepositoryProvider(
          create: (context) => Api(),
        ),
        RepositoryProvider(
          create: (context) => RepoSettings(),
        ),
        RepositoryProvider(
          create: (context) => RepoCharacters(
            api: RepositoryProvider.of<Api>(context),
          ),
        ),
        ChangeNotifierProvider<NavBarViewModel>(
          create: (_) => NavBarViewModel()..init(),
        ),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => BlockCharacter(
              repo: RepositoryProvider.of<RepoCharacters>(context),
            )..add(EventCharactersFilterByName('')),
          ),
        ],
        child: child,
      ),
    );
  }
}
