import 'package:as_lesson_1/constants/app_assets.dart';
import 'package:as_lesson_1/generated/l10n.dart';
import 'package:flutter/material.dart';

enum NavBarModelType { characters, settings }

class NavBarViewModel extends ChangeNotifier {
  late int _index;
  late final List<GlobalKey<NavigatorState>> keys;
  late final List<NavBarModel> models;

  void init() {
    _index = 0;

    keys = [
      GlobalKey<NavigatorState>(),
      GlobalKey<NavigatorState>(),
    ];

    models = [
      NavBarModel(
        type: NavBarModelType.characters,
        svgImgPath: AppAssets.svg.characters,
      ),
      NavBarModel(
        type: NavBarModelType.settings,
        svgImgPath: AppAssets.svg.settings,
      ),
    ];

    models[_index].isActive = true;
  }

  int get index => _index;

  set index(int value) {
    if (_index != value) {
      models[_index].isActive = false;

      _index = value;

      models[_index].isActive = true;

      notifyListeners();
    }
  }

  void changeBadgeValue({
    required NavBarModelType type,
    required bool value,
  }) {
    switch (type) {
      case NavBarModelType.characters:
        models
            .firstWhere((e) => e.type == NavBarModelType.characters)
            .isBadged = value;
        break;
      case NavBarModelType.settings:
        models.firstWhere((e) => e.type == NavBarModelType.settings).isBadged =
            value;
        break;
    }

    notifyListeners();
  }
}

class NavBarModel {
  final NavBarModelType type;
  final String svgImgPath;
  bool isBadged;
  bool isActive;

  NavBarModel({
    required this.svgImgPath,
    required this.type,
    this.isActive = false,
    this.isBadged = false,
  });

  String title(S locale) {
    switch (type) {
      case NavBarModelType.characters:
        return locale.characters;

      case NavBarModelType.settings:
        return locale.settings;
    }
  }
}
