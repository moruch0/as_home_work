import 'package:as_lesson_1/constants/app_colors.dart';
import 'package:as_lesson_1/screens/characters_screen/feature.dart';
import 'package:as_lesson_1/screens/settings_screen.dart';
import 'package:as_lesson_1/widgets/app_nav_bar/view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../generated/l10n.dart';

class AppNavBar extends StatelessWidget {
  const AppNavBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: context.watch<NavBarViewModel>().index,
        children: [
          Navigator(
            key: context.watch<NavBarViewModel>().keys[0],
            onGenerateRoute: (settings) => charactersRoute(),
          ),
          Navigator(
            key: context.watch<NavBarViewModel>().keys[1],
            onGenerateRoute: (settings) =>
                MaterialPageRoute(builder: (contextRoute) {
              return const SettingsScreen();
            }),
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: context.watch<NavBarViewModel>().index,
        selectedItemColor: AppColors.primaryIndigo,
        unselectedItemColor: AppColors.gray_4,
        showSelectedLabels: true,
        showUnselectedLabels: true,
        type: BottomNavigationBarType.fixed,
        onTap: (index) => context.read<NavBarViewModel>().index = index,
        items: context
            .read<NavBarViewModel>()
            .models
            .map(
              (e) => BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  e.svgImgPath,
                  color:
                      e.isActive ? AppColors.primaryIndigo : AppColors.gray_4,
                ),
                label: e.title(S.of(context)),
              ),
            )
            .toList(),
      ),
    );
  }
}
