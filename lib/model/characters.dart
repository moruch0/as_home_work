class Character {
  final String name;
  final String species;
  final String status;
  final String gender;
  final String? image;
  final bool alive;
  const Character({
    required this.name,
    required this.species,
    required this.gender,
    required this.status,
    this.alive = true,
    this.image,
  });

  static fromJson(item) {}
}
