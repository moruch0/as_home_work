import 'package:as_lesson_1/common/repository/repo_settings.dart';
import 'package:as_lesson_1/constants/app_colors.dart';
import 'package:as_lesson_1/generated/l10n.dart';
import 'package:as_lesson_1/widgets/app_nav_bar/app_nav_bar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../constants/app_styles.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.background,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        centerTitle: false,
        title: Text(
          S.of(context).settings,
          style: AppStyles.settingsText,
        ),
      ),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "${S.of(context).language}: ",
              style: const TextStyle(
                fontSize: 18,
              ),
            ),
            DropdownButton(
              value: Intl.getCurrentLocale(),
              items: [
                DropdownMenuItem(
                  value: 'en',
                  child: Text(S.of(context).english),
                ),
                DropdownMenuItem(
                  value: 'ru_RU',
                  child: Text(S.of(context).russian),
                ),
              ],
              onChanged: (String? value) async {
                if (value == null) return;
                if (value == 'en') {
                  await S.load(const Locale('en'));
                  setState(() {});
                } else if (value == 'ru_RU') {
                  await S.load(const Locale('ru', 'RU'));
                  setState(() {});
                }
                final repoSettings =
                    Provider.of<RepoSettings>(context, listen: false);

                repoSettings.saveLocal(value);
              },
            ),
          ],
        ),
      ),
    );
  }
}
