import 'package:as_lesson_1/constants/app_assets.dart';
import 'package:as_lesson_1/constants/app_colors.dart';
import 'package:as_lesson_1/model/characters.dart';
import 'package:flutter/material.dart';

import '../../../../constants/app_styles.dart';

class CharactersGridView extends StatelessWidget {
  final List<Character> characters;
  const CharactersGridView({Key? key, required this.characters})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      primary: false,
      padding: const EdgeInsets.symmetric(horizontal: 16),
      crossAxisSpacing: 6,
      mainAxisSpacing: 24,
      crossAxisCount: 2,
      children: characters
          .map((e) => _Character(
                name: e.name,
                species: e.species,
                gender: e.gender,
                status: e.status,
                image: e.image,
              ))
          .toList(),
    );
  }
}

class _Character extends StatelessWidget {
  final String name;
  final String species;
  final String status;
  final String gender;
  final String? image;
  final bool alive;
  const _Character({
    Key? key,
    required this.name,
    required this.species,
    required this.gender,
    required this.status,
    this.alive = true,
    this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CircleAvatar(
          minRadius: 42,
          maxRadius: 52,
          backgroundImage: image == null
              ? AssetImage(AppAssets.images.noAvatar) as ImageProvider
              : NetworkImage(image!),
        ),
        const SizedBox(
          height: 18,
        ),
        Text(
          status,
          style: alive
              ? AppStyles.additionalText
              : AppStyles.additionalText.copyWith(
                  color: AppColors.red,
                ),
        ),
        Text(
          name,
          style: AppStyles.mediumText.copyWith(fontSize: 14),
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
        ),
        Text(
          '$species, $gender',
          style: AppStyles.subtitleText,
        ),
      ],
    );
  }
}
