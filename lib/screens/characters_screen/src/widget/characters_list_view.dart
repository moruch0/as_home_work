import 'package:as_lesson_1/constants/app_assets.dart';
import 'package:as_lesson_1/constants/app_colors.dart';
import 'package:as_lesson_1/model/characters.dart';
import 'package:flutter/material.dart';

import '../../../../constants/app_styles.dart';

class CharactersListView extends StatelessWidget {
  final List<Character> characters;
  const CharactersListView({Key? key, required this.characters})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      children: characters
          .map((e) => _Character(
                name: e.name,
                species: e.species,
                gender: e.gender,
                status: e.status,
                image: e.image,
              ))
          .toList(),
    );
  }
}

class _Character extends StatelessWidget {
  final String name;
  final String species;
  final String status;
  final String gender;
  final String? image;
  final bool alive;
  const _Character({
    Key? key,
    required this.name,
    required this.species,
    required this.gender,
    required this.status,
    this.alive = true,
    this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 24),
      child: Row(
        children: [
          CircleAvatar(
            minRadius: 40,
            maxRadius: 40,
            backgroundImage: image == null
                ? AssetImage(AppAssets.images.noAvatar) as ImageProvider
                : NetworkImage(image!),
          ),
          const SizedBox(
            width: 18,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                status,
                style: alive
                    ? AppStyles.additionalText
                    : AppStyles.additionalText.copyWith(
                        color: AppColors.red,
                      ),
              ),
              Text(
                name,
                style: AppStyles.mediumText,
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              ),
              Text(
                '$species, $gender',
                style: AppStyles.subtitleText,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
