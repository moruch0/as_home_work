
import 'package:as_lesson_1/constants/app_colors.dart';
import 'package:as_lesson_1/generated/l10n.dart';
import 'package:flutter/material.dart';

import '../../../../constants/app_styles.dart';

class SearchField extends StatelessWidget {
  final ValueChanged<String>? onChanged;
  const SearchField({Key? key, this.onChanged}) : super(key: key);

  @override
  Widget build(BuildContext context) {
   

    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              style: AppStyles.primaryText,
              cursorColor: AppColors.gray_4,
              onChanged: onChanged,
              decoration: InputDecoration(
                hintText: S.of(context).find_a_character,
                hintStyle: AppStyles.mediumText.copyWith(
                  color: AppColors.gray_4,
                ),
                contentPadding: const EdgeInsets.symmetric(
                  vertical: 14.0,
                  horizontal: 18.0,
                ),
                filled: true,
                fillColor: AppColors.gray_6,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30),
                  borderSide: BorderSide.none,
                ),
                prefixIcon: GestureDetector(
                  onTap: () {
                    print("prefixIcon");
                  },
                  child: const Icon(
                    Icons.search,
                    size: 24,
                    color: AppColors.icon,
                  ),
                ),
                suffixIcon: GestureDetector(
                  onTap: () {
                    print("suffixIcon");
                  },
                  child: const Icon(
                    Icons.filter_list_alt,
                    size: 24,
                    color: AppColors.icon,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
