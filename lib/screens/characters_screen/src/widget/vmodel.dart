import 'package:as_lesson_1/common/models/character_model.dart';
import 'package:as_lesson_1/model/characters.dart';
import 'package:flutter/material.dart';

class PersonListVModel with ChangeNotifier {
  var filteredList = <CharacterDto>[];
  var isListView = true;
  var isLoading = true;
  var _countCharacters = 0;

  var _charactersList = <Character>[];

  set charactersList(List<Character> characters) {
    _charactersList = characters;
    notifyListeners();
  }

  set countCharacters(int count) {
    _countCharacters = count;
    notifyListeners();
  }

  int get countCharacters => _countCharacters;

  List<Character> get charactersList => _charactersList;

  void switchView() {
    isListView = !isListView;
    notifyListeners();
  }
}
