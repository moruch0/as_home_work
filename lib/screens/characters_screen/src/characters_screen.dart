import 'package:as_lesson_1/constants/app_colors.dart';
import 'package:as_lesson_1/constants/app_styles.dart';
import 'package:as_lesson_1/generated/l10n.dart';
import 'package:as_lesson_1/screens/characters_screen/src/bloc/bloc_characters.dart';
import 'package:as_lesson_1/screens/characters_screen/src/widget/characters_grid_view.dart';
import 'package:as_lesson_1/screens/characters_screen/src/widget/characters_list_view.dart';
import 'package:as_lesson_1/screens/characters_screen/src/widget/search_field.dart';
import 'package:as_lesson_1/screens/characters_screen/src/widget/vmodel.dart';

import 'package:as_lesson_1/widgets/app_nav_bar/app_nav_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CharactersScreen extends StatelessWidget {
  const CharactersScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var vm = context.watch<PersonListVModel>();

    return Scaffold(
      body: Column(
        children: [
          SizedBox(height: MediaQuery.of(context).padding.top),
          SearchField(
            onChanged: (value) => context.read<BlockCharacter>()
              ..add(EventCharactersFilterByName(value)),
          ),
          BlocConsumer<BlockCharacter, StateBlocCharacters>(
            buildWhen: (previous, current) => current is StateCharactersData,
            listener: (context, state) {
              if (state is StateCharactersLoading) {
                vm.isLoading = state.isLoading;
              }
              if (state is StateCharactersData) {
                vm.charactersList = state.data;
                vm.countCharacters = state.data.length;
              }
            },
            builder: (context, state) {
              return Expanded(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            '${S.of(context).total_characters}: ${vm.countCharacters}',
                            style: AppStyles.topTitle,
                          ),
                          IconButton(
                            icon: Icon(vm.isListView
                                ? Icons.format_list_bulleted
                                : Icons.grid_view),
                            color: AppColors.icon,
                            onPressed: () {
                              vm.switchView();
                              // isBloc = !isBloc;
                            },
                          ),
                        ],
                      ),
                    ),
                    vm.isLoading
                        ? Expanded(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: const [
                                CircularProgressIndicator(
                                  backgroundColor: AppColors.gray_4,
                                )
                              ],
                            ),
                          )
                        : vm.charactersList.isEmpty
                            ? Expanded(
                                child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Flexible(
                                      child: Text(
                                          S.of(context).no_characters_found))
                                ],
                              ))
                            : Expanded(
                                child: vm.isListView
                                    ? CharactersListView(
                                        characters: vm.charactersList,
                                      )
                                    : CharactersGridView(
                                        characters: vm.charactersList,
                                      ),
                              )
                  ],
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
