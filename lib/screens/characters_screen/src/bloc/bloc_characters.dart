import 'package:as_lesson_1/common/repository/repo_characters.dart';
import 'package:as_lesson_1/model/characters.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'states.dart';
part 'events.dart';

class BlockCharacter extends Bloc<EventBlocCharacters, StateBlocCharacters> {
  final RepoCharacters repo;
  BlockCharacter({
    required this.repo,
  }) : super(StateCharactersInitial()) {
    on<EventCharactersFilterByName>((event, emit) async {
      emit(StateCharactersLoading(true));
      final result = await repo.filterByName(event.name);

      if (result.errorMesage != null) {
        emit(
          StateCharactersError(result.errorMesage!),
        );
        return;
      }

      var char = result.characterList
          ?.map((value) => Character(
                gender: value.gender ?? "",
                name: value.name ?? "",
                species: value.species ?? "",
                status: value.status ?? "",
                image: value.image,
              ))
          .toList();

      emit(StateCharactersLoading(false));
      emit(StateCharactersData(data: char ?? []));
    });
  }
}
