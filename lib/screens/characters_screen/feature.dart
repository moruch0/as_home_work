import 'package:as_lesson_1/common/repository/repo_characters.dart';
import 'package:as_lesson_1/screens/characters_screen/src/bloc/bloc_characters.dart';
import 'package:as_lesson_1/screens/characters_screen/src/characters_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'src/widget/vmodel.dart';

MaterialPageRoute charactersRoute() {
  return MaterialPageRoute(builder: (contextRoute) {
    return const CharactersFeature();
  });
}

class CharactersFeature extends StatelessWidget {
  const CharactersFeature({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<PersonListVModel>(
      create: (_) => PersonListVModel(),
      child: const CharactersScreen(),
    );
  }
}
